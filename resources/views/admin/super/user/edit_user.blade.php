@extends('admin.layouts.app')

@section('section_title')
    Edit Admin
@endsection

@section('content')

    <div class="row">
        <div class="col-md-7">
            @include('errors.errors')
            <form method="POST" action="{{ route('update_user', ['user_id' =>$user->id]) }}">
                @csrf
                <div class="form-group row">
                    <label for="first_name" class="col-md-5 col-form-label text-md-right">
                        {{ __('District') }} *
                    </label>
                    <div class="col-md-6">
                        <select class="form-control{{ $errors->has('district') ? ' is-invalid' : '' }}"
                                name="district"
                                required
                        >
                            <option>- Sélection votre Circonscription -</option>
                            @foreach($districts as $district)
                                <option value="{{ $district->id }}" @if($user->district_id == $district->id) selected @endif >{{ $district->name_en }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-md-5 col-form-label text-md-right">
                        {{ __('User Name') }} *
                    </label>
                    <div class="col-md-6">
                        <input id="name"
                               type="text"
                               class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                               name="name"
                               value="{{ old('name')?old('name'):$user->name }}" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="email" class="col-md-5 col-form-label text-md-right">{{ __('E-Mail Address *') }}</label>
                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email')?old('email'):$user->email }}" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="password" class="col-md-5 col-form-label text-md-right">{{ __('Password *') }}</label>
                    <div class="col-md-6">
                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                    </div>
                </div>
                <hr>
                <div class="form-group row">
                    <label for="first_name" class="col-md-5 col-form-label text-md-right">
                        {{ __('Privilege') }} *
                    </label>
                    <div class="col-md-6">
                        <select class="form-control{{ $errors->has('role_id') ? ' is-invalid' : '' }}"
                                name="role_id"
                                required
                        >
                            <option>- Select User Type -</option>
                            @foreach($user_types as $user_type)
                                <option value="{{ $user_type->id }}" @if($user->role_id == $user_type->id) selected @endif>{{ $user_type->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <hr>
                <div class="form-group row">
                    <div class="col-md-6 offset-5">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Edit') }}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection
