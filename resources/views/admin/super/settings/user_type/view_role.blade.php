@extends('admin.layouts.app')

@section('section_title')
    {{ $role->name }}
@endsection

@section('content')

    <div class="row">
        <div class="col-md-8">
            <div class="form-group">
                <label class="col-form-label text-md-right">Super Admin</label>
            </div>
            <div class="row">
                <div class="form-group col-md-2">
                    @if($role->super_admin)
                        <i class="fa fa-check" aria-hidden="true"></i>
                    @else
                        <i class="fa fa-circle-thin" aria-hidden="true"></i>
                    @endif
                    Yes!
                </div>
                <div class="form-group col-md-2">
                    @if(!$role->super_admin)
                        <i class="fa fa-check" aria-hidden="true"></i>
                    @else
                        <i class="fa fa-circle-thin" aria-hidden="true"></i>
                    @endif
                    No!
                </div>
            </div>
            @if(!$role->super_admin)
                <div id="privileges_configuration" class="form-group">
                    <label>Privileges Configuration</label>
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                        <tr class="active">
                            <th width="3%">No.</th>
                            <th width="60%">Module's Name</th>
                            <th>View</th><th>Create</th><th>Read</th><th>Update</th><th>Delete</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>New Ticket</td>
                            <td class="alert-active" align="center">
                                @if(isset($permissions->new_request->is_visible))
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                            </td>
                            <td class="alert-warning" align="center">
                                @if(isset($permissions->new_request->is_create))
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                            </td>
                            <td class="alert-info" align="center">
                                @if(isset($permissions->new_request->is_read))
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                            </td>
                            <td class="alert-success" align="center">
                                @if(isset($permissions->new_request->is_edit))
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                            </td>
                            <td class="alert-danger" align="center">
                                @if(isset($permissions->new_request->is_delete))
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>All Tickets	</td>
                            <td class="alert-active" align="center">
                                @if(isset($permissions->all_requests->is_visible))
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                            </td>
                            <td class="alert-warning" align="center">
                                @if(isset($permissions->all_requests->is_create))
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                            </td>
                            <td class="alert-info" align="center">
                                @if(isset($permissions->all_requests->is_read))
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                            </td>
                            <td class="alert-success" align="center">
                                @if(isset($permissions->all_requests->is_edit))
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                            </td>
                            <td class="alert-danger" align="center">
                                @if(isset($permissions->all_requests->is_delete))
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Selected Tickets</td>
                            <td class="alert-active" align="center">
                                @if(isset($permissions->selected_request->is_visible))
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                            </td>
                            <td class="alert-warning" align="center">
                                @if(isset($permissions->selected_request->is_create))
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                            </td>
                            <td class="alert-info" align="center">
                                @if(isset($permissions->selected_request->is_read))
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                            </td>
                            <td class="alert-success" align="center">
                                @if(isset($permissions->selected_request->is_edit))
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                            </td>
                            <td class="alert-danger" align="center">
                                @if(isset($permissions->selected_request->is_delete))
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>Closed Ticket</td>
                            <td class="alert-active" align="center">
                                @if(isset($permissions->closed_request->is_visible))
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                            </td>
                            <td class="alert-warning" align="center">
                                @if(isset($permissions->closed_request->is_create))
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                            </td>
                            <td class="alert-info" align="center">
                                @if(isset($permissions->closed_request->is_read))
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                            </td>
                            <td class="alert-success" align="center">
                                @if(isset($permissions->closed_request->is_edit))
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                            </td>
                            <td class="alert-danger" align="center">
                                @if(isset($permissions->closed_request->is_delete))
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>Applicant</td>
                            <td class="alert-active" align="center">
                                @if(isset($permissions->applicant->is_visible))
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                            </td>
                            <td class="alert-warning" align="center">
                                @if(isset($permissions->applicant->is_create))
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                            </td>
                            <td class="alert-info" align="center">
                                @if(isset($permissions->applicant->is_read))
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                            </td>
                            <td class="alert-success" align="center">
                                @if(isset($permissions->applicant->is_edit))
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                            </td>
                            <td class="alert-danger" align="center">
                                @if(isset($permissions->applicant->is_delete))
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>6</td>
                            <td>Configuration (districts status Categories)</td>
                            <td class="alert-active" align="center">
                                @if(isset($permissions->settings->is_visible))
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                            </td>
                            <td class="alert-warning" align="center">
                                @if(isset($permissions->settings->is_create))
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                            </td>
                            <td class="alert-info" align="center">
                                @if(isset($permissions->settings->is_read))
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                            </td>
                            <td class="alert-success" align="center">
                                @if(isset($permissions->settings->is_edit))
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                            </td>
                            <td class="alert-danger" align="center">
                                @if(isset($permissions->settings->is_delete))
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>7</td>
                            <td>User settings (user management + profiles)</td>
                            <td class="alert-active" align="center">
                                @if(isset($permissions->user_management->is_visible))
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                            </td>
                            <td class="alert-warning" align="center">
                                @if(isset($permissions->user_management->is_create))
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                            </td>
                            <td class="alert-info" align="center">
                                @if(isset($permissions->user_management->is_read))
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                            </td>
                            <td class="alert-success" align="center">
                                @if(isset($permissions->user_management->is_edit))
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                            </td>
                            <td class="alert-danger" align="center">
                                @if(isset($permissions->user_management->is_delete))
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>8</td>
                            <td>Privilege Roles</td>
                            <td class="alert-active" align="center">
                                @if(isset($permissions->roles->is_visible))
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                            </td>
                            <td class="alert-warning" align="center">
                                @if(isset($permissions->roles->is_create))
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                            </td>
                            <td class="alert-info" align="center">
                                @if(isset($permissions->roles->is_read))
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                            </td>
                            <td class="alert-success" align="center">
                                @if(isset($permissions->roles->is_edit))
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                            </td>
                            <td class="alert-danger" align="center">
                                @if(isset($permissions->roles->is_delete))
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>9</td>
                            <td>Gov request</td>
                            <td class="alert-active" align="center">
                                @if(isset($permissions->gov_request->is_visible))
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                            </td>
                            <td class="alert-warning" align="center">
                                @if(isset($permissions->gov_request->is_create))
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                            </td>
                            <td class="alert-info" align="center">
                                @if(isset($permissions->gov_request->is_read))
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                            </td>
                            <td class="alert-success" align="center">
                                @if(isset($permissions->gov_request->is_edit))
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                            </td>
                            <td class="alert-danger" align="center">
                                @if(isset($permissions->gov_request->is_delete))
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            @endif
        </div>
    </div>

@endsection
