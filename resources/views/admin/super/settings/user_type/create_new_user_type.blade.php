@extends('admin.layouts.app')

@section('section_title')
    Add New Role
@endsection

@section('content')

    @include('errors.errors')
    @if($flash = session('message'))
        <div class="alert alert-success">
            <i class="fa fa-bell" aria-hidden="true"></i>
            {{ $flash }}
        </div>
    @endif
    <div class="row">
        <div class="col-md-8">
            <form action="{{ route('store_user_type') }}" method="post">
                {{ csrf_field() }}
                <div class="form-group">
                    <label class="col-form-label text-md-right">Name</label>
                    <input type="text" name="name" class="form-control" required autofocus>
                </div>
                <roles-component></roles-component>
                <div>
                    <button class="btn btn-primary">Add New One</button>
                </div>
            </form>
        </div>
    </div>

@endsection
