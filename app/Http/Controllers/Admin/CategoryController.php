<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function index()
    {
        return view('admin.super.settings.category.show_categories');
    }

    public function create()
    {
        return view('admin.super.settings.category.create_category');
    }

    public function store(Request $request)
    {
        $this->validate(
            $request,
            [
                'name_ar' => 'required',
                'name_fr' => 'required',
                'name_en' => 'required',
                'active' => 'required'
            ]
        );
        Category::create(
            [
                'name_ar' => $request->name_ar,
                'name_fr' => $request->name_fr,
                'name_en' => $request->name_en,
                'active' => $request->active
            ]
        );
        session()->flash('message', 'A new category has been created!');
        return redirect()->route('categories');
    }

    public function remove($category_id)
    {
        $category = Category::where('id', $category_id)->first();
        $category->delete();
        session()->flash('message', 'A category has been removed!');
        return redirect()->route('categories');
    }

}
