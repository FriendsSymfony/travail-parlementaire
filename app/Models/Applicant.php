<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;

class Applicant extends Model
{

    protected $fillable = ['cin', 'name', 'gender', 'code_number', 'phone_number', 'email', 'user_id'];

    public function requests()
    {
        return $this->hasMany(Request::class, 'applicant_id', 'id');
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = Crypt::encryptString($value);
    }

    public function getNameAttribute($value)
    {
        return Crypt::decryptString($value);
    }

    public function setGenderAttribute($value)
    {
        $this->attributes['gender'] = Crypt::encryptString($value);
    }

    public function getGenderAttribute($value)
    {
        return Crypt::decryptString($value);
    }

    public function setEmailAttribute($value)
    {
        $this->attributes['email'] = Crypt::encryptString($value);
    }

    public function getEmailAttribute($value)
    {
        return Crypt::decryptString($value);
    }

    public function setPhoneNumberAttribute($value)
    {
        $this->attributes['phone_number'] = Crypt::encryptString($value);
    }

    public function getPhoneNumberAttribute($value)
    {
        return Crypt::decryptString($value);
    }
    /*
    public function setCinAttribute($value)
    {
        $this->attributes['cin'] = $this->encrypt($value);
    }

    public function getCinAttribute($value)
    {
        return $this->decrypt($value);
    }

    private function encrypt($value)
    {
        $tab = [1, 5, 9, 7, 3, 4, 2, 6, 8, 0];
        $new = '';
        foreach ($value as $key => $item) {
            if ($new == '') {
                $new .= (int) $item + $tab[$key];
            } else {
                $new .= ',' . $tab[$item];
            }
        }

        return $new;
    }

    private function decrypt(string $value)
    {
        $tab = [1, 5, 9, 7, 3, 4, 2, 6, 8, 0];
        $new = '';
        $value = explode(',', $value);
        foreach ($value as $key => $item) {
            $new .= (string)($value[$key] - $tab[$key]);
        }

        return $new;
    }
    */
}
